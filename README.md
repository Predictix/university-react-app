# README #


##Project Goal


The goal of this project is to showcase all the phases and artifacts involved in the development of a web application using the LogicBlox platform and ReactJS/HTML/CSS.

We have chosen a very simple data model because the focus is not on the model itself as much as it’s about the work that’s involved in doing it.

##Project Description
The web application will allow users to search for students using their first or last names by presenting them with a screen that contains a field, a search button and a RTN (Return To Null) button.

Once the user entered a first or a last name and hits the search button, a grid/data table will show up the list of all the students that match the search criteria. The grid will show the student_id, first name, last name, their immatriculation data (the day they enrolled in the university) as well as their GPA (average of all grades for the courses they took). E.g:

If a student took: CS101 and CS102 and they got the following grades for them: 3.0 and 4.0 respectively then their GPA will be: (3.0 * 1 + 4.0 * 1)/2 =  3.5

If no student was found to match the search criteria, a message telling the user that no students found will appear instead.

In the case where students matching the search criteria were found, the user can click on 1 student from the Students table to display another table (the courses table) that show the list of all courses taken by the student. The courses table will show the course_id, it’s name (e.g: CS101’s name is “Introduction to Databases”, CS102’s name is “Compilers”...), the date the student started taking that course and the date the student finished the course as well the grade of the student in that course. If a student is still taking the course then the last two field can be left empty.



### How do I get set up? ###

##Setup

* Download dependencies :

```
lb services stop    # if already started
./scripts/get-deps
```
* Setup development and build environment :

```
source scripts/env.sh
```
* Restart LB services :
```
lb services start
```
* In /backend execute :
```
lb config
```
* then to build your backend execute :
```
make
```
* To fetch the frontend dependencies the first time, get into ./frontend directory and run :
```
npm install
```
* Run the UI :
```
npm start
```

You can now access to your application by entering "http://localhost:3000" in your browser


### Who do I talk to? ###

* Marouen Marzouki
* Ghaith M'hamdi
