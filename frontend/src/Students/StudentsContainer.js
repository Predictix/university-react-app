import React from 'react';
import "IR-style-guide/styleGuide-standalone.css";
import PropTypes from "prop-types";
import axios from 'axios';
import { StudentsComponent } from './StudentsComponent';
import { SearchForm } from './SearchForm';
import { CoursesContainer } from '../Courses/CoursesContainer';
import { VerticalRhythm } from 'IR-style-guide';


export class StudentsContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rowData: [],
            studentSearched: "",
            displayGrid: false,
            studentID: "",
            displayCourses: false
        }
        this.toggleGrid = this.toggleGrid.bind(this);
        this.getTheStudentSearched = this.getTheStudentSearched.bind(this);
        this.getTheStudentID = this.getTheStudentID.bind(this);
    }

    toggleGrid() {
        this.setState({ displayGrid: !this.state.displayGrid })
    }

    getTheStudentSearched(studentSearched) {
        this.setState({ studentSearched })
    }

    getTheStudentID(studentID) {
        this.setState({ studentID });
        this.setState({ displayCourses: true });
        this.forceUpdate();
    }

    sendRequest() {
        if (this.state.displayGrid) {
            const that = this;
            let term = this.state.studentSearched;
            axios.post('/dw/searchStudent', { "term": term })
                .then(function (response) {
                    if (response.data && response.data.student) {
                        const rowData = response.data.student.map(student => ({
                            Student_id: student.id,
                            Student_first_name: student.first_name,
                            Student_last_name: student.last_name,
                            Student_enrollement_date: student.enrollement_date,
                            Student_gpa: student.gpa,
                        }))
                        if (rowData === []) that.setState({ rowData: null });
                        else that.setState({ rowData });
                    } else that.setState({ rowData: null });
                })
                .catch(function (error) {
                    console.log("response", error);

                });
        }
    }




    render() {
        if ((this.state.rowData) && (this.state.rowData.length == 0)) {
            this.sendRequest();
        }
        return (
            <div>

                <div>
                    <SearchForm getTheStudentSearched={this.getTheStudentSearched} onClick={this.toggleGrid} displayGrid={this.state.displayGrid} />
                </div>

                <div>

                    <VerticalRhythm spacing="4">

                        <StudentsComponent rowData={this.state.rowData} displayGrid={!this.state.displayGrid} getTheStudentID={(id) => { this.getTheStudentID(id) }} />

                    </VerticalRhythm>
                </div>

                <div>

                    <CoursesContainer id_req={this.state.studentID} displayCourses={this.state.displayCourses} />

                </div>

            </div>
        );
    }



}
