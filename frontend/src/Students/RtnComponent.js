import React from 'react';
import "IR-style-guide/styleGuide-standalone.css";
import '../General/App.css';
import PropTypes from "prop-types";

import { Button } from 'IR-style-guide';

export class RtnComponent extends React.Component {
    restart() {
        window.location.reload();
    }
    render() {
        return (
            <div id='RtnComponent' style={{ textAlign: "center" }} hidden={this.props.hideShow}>
                <Button presentation={Button.PRESENTATION.SECONDARY} onClick={this.restart}>RTN</Button>
            </div>


        );


    }
}

