import React from 'react';
import "IR-style-guide/styleGuide-standalone.css";
import { RtnComponent } from './RtnComponent';
//import ReactDOM from 'react-dom';
//import logo from './logo.svg';
import '../General/App.css';
import PropTypes from "prop-types";
import { TextField, FormControl, VerticalRhythm, HorizontalList, Button } from 'IR-style-guide';

export class SearchForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            studentSearched: "",
        };

        this.handleChange = this.handleChange.bind(this);

    }


    handleChange(e) {
        this.props.getTheStudentSearched(e.target.value);
    }

    render() {
        return (
            <div style={{ margin: "0 20% 0 35%" }}>
                <div id="textfield" >
                    <VerticalRhythm spacing="4">
                        <FormControl label="" >
                            <TextField name="normal" placeholder="Please provide first/last name" id="userInput" onChange={this.handleChange} />
                        </FormControl>
                    </VerticalRhythm >
                </div>
                <VerticalRhythm spacing="4">
                    <HorizontalList
                        presentation={HorizontalList.PRESENTATION.UNMARKED}
                        semantic={HorizontalList.SEMANTIC.UNORDERED}
                        spacing={HorizontalList.SPACING.INCREMENT_4_X}
                    >

                        <HorizontalList.Item>
                            <div id="search button"  >
                                <Button presentation={Button.PRESENTATION.SECONDARY} onClick={this.props.onClick} isDisabled={ this.props.displayGrid }>Search</Button>
                            </div>
                        </HorizontalList.Item>
                        <HorizontalList.Item>
                            <RtnComponent hideShow={!this.props.displayGrid} />
                        </HorizontalList.Item>
                    </HorizontalList>

                </VerticalRhythm>

            </div>


        );


    }
}


