import React from 'react';
import "IR-style-guide/styleGuide-standalone.css";
import PropTypes from "prop-types";
import { DataTable, LoadingIndicator } from 'IR-style-guide';


export class StudentsComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    getDataTableColumnsConfig() {
        return [
            {
                field: "Student_id",
                headerName: "Id",

            },


            {
                field: "Student_first_name",
                headerName: "First Name",
                width: 300
            },
            {
                field: "Student_last_name",
                headerName: "Last Name",
                width: 300
            },

            {
                field: "Student_enrollement_date",
                headerName: "Enrollement date",
                width: 300
            },

            {
                field: "Student_gpa",
                headerName: "GPA",
                width: 300
            },
        ];
    }

    onSelectionChanged(e) {
        this.props.getTheStudentID(e.node.data.Student_id);
    }

    componentWillMount() {
        return (
            <div>

                <LoadingIndicator horizontalAlignment={LoadingIndicator.HORIZONTAL_ALIGNMENT.CENTER}
                    verticalAlignment={LoadingIndicator.VERTICAL_ALIGNMENT.TOP}
                />

            </div>
        );
    }



    render() {
        if (!this.props.displayGrid) {
            return (
                <div className="ag-fresh" style={{ height: "200px" }}>
                    <DataTable
                        columnDefs={this.getDataTableColumnsConfig()}
                        rowData={this.props.rowData}
                        enableColResize="true"
                        onGridReady={function (params) { params.api.sizeColumnsToFit() }}
                        rowSelection="single"
                        onRowSelected={this.onSelectionChanged.bind(this)}
                    />
                </div>
            );

        }
        else return null;

    }
}