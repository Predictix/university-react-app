import React from 'react';
import "IR-style-guide/styleGuide-standalone.css";
import PropTypes from "prop-types";
import { DataTable } from 'IR-style-guide';
import { CoursesComponent } from './CoursesComponent';
import axios from 'axios';


export class CoursesContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rowData: [],
            ready: false
        }
    }

    sendRequest() {
        const that = this;
        let id_req = this.props.id_req;
        axios.post('/dw/getMarks', { "id_req": id_req })
            .then(function (response) {
                const rowData = response.data.course.map(course => ({
                    Course_id: course.Course_id,
                    Course_name: course.Course_name,
                    Course_start_date: course.Start_date,
                    Course_end_date: course.End_date,
                    Grade: course.Grade,
                }))
                that.setState({
                    rowData
                });
                //
            })
            .catch(function (error) {
                console.log(error);
            });

    }

    componentWillReceiveProps() {
        this.sendRequest();
    }


    render() {
        return (
            <div hidden={!this.props.displayCourses}>
                <CoursesComponent rowData={this.state.rowData} />
            </div>
        );
    }



}