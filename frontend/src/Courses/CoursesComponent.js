import React from 'react';
import "IR-style-guide/styleGuide-standalone.css";
import { DataTable,LoadingIndicator } from 'IR-style-guide';


export class CoursesComponent extends React.Component {
    constructor(props) {
        super(props);
    }



    getDataTableColumnsConfig() {
        return [
            {
                field: "Course_id",
                headerName: "Id",

            },
            {
                field: "Course_name",
                headerName: "Course Name",
                width: 300
            },

            {
                field: "Course_start_date",
                headerName: "Start Date",
                width: 300
            },
            {
                field: "Course_end_date",
                headerName: "End date",
                width: 300
            },

            {
                field: "Grade",
                headerName: "Grade",
                width: 300
            },
        ];
    }


    componentWillMount() {
        return (
            <div>

                <LoadingIndicator horizontalAlignment={LoadingIndicator.HORIZONTAL_ALIGNMENT.CENTER}
                    verticalAlignment={LoadingIndicator.VERTICAL_ALIGNMENT.TOP}
                />

            </div>
        );
    }

    renderCondition() {
        if (this.props.rowData.length == 0) {
            return (
                <div style={{ color: "red" }} >

                    <h3> The student does not have courses </h3>

                </div>
            );

        }
        else {
            return (
                <div>
                    <div className="ag-fresh" style={{ height: "200px" }}>
                        <DataTable
                            columnDefs={this.getDataTableColumnsConfig()}
                            rowData={this.props.rowData}
                            enableColResize="true"
                            onGridReady={function (params) { params.api.sizeColumnsToFit() }}
                        />
                    </div>
                </div>
            );

        }
    }

    render() {
        return (
            <div>
                {this.renderCondition()}
            </div>
        );
    }



}