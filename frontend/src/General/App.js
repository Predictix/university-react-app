import React from 'react';
import "IR-style-guide/styleGuide-standalone.css";

//import ReactDOM from 'react-dom';
//import logo from './logo.svg';
import './App.css';
import { Header, VerticalRhythm } from 'IR-style-guide';
import { StudentsContainer } from '../Students/StudentsContainer';




class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };

  }


  render() {

    return (
      <div id="general_design" style={{ textAlign: "center" }}>
        <VerticalRhythm spacing="4">
          <div id="home_head" style={{ display: 'flex', justifyContent: 'center' }}>
            <Header title="University"></Header>
          </div>
        </VerticalRhythm>

        <VerticalRhythm spacing="4">
          <StudentsContainer />
        </VerticalRhythm>


      </div>

    );
  }
}

export default App;
