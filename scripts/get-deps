#!/usr/bin/env bash

set -e

### Configuration

green='\e[0;32m'
nocol='\e[0m'
BUILDER_URL="https://bob.logicblox.com/"
LOGICBLOX_RELEASE=$BUILDER_URL"job/logicblox-40/release-4.4.1/linux.binary_tarball/latest/download/1"

arch=`uname | tr '[:upper:]' '[:lower:]'`

### Functions

function clean()
{
    if [ -d deps ]
    then
        echo -e "${green}--------------------------------"
        echo -e " Deleting old deps..."
        echo -e "--------------------------------${nocol}"
        chmod -R +w deps
        rm -rf deps
    fi
}

function cleanup()
{
    if [ -d deps/downloads ]
    then
        echo -e "${green}--------------------------------"
            echo -e " Cleaning Up..."
        echo -e "--------------------------------${nocol}"
        rm -rf deps/downloads
    fi
}

function create_deps()
{
    if [ ! -d deps -o ! -d deps/downloads ]
    then
        echo -e "${green}--------------------------------"
            echo -e " Creating deps directories..."
        echo -e "--------------------------------${nocol}"
        mkdir -p deps/downloads
    fi
}

function logicblox()
{
    pushd deps 1>/dev/null
    echo -e "${green}--------------------------------"
    echo -e " Downloading logicblox release..."
    echo -e "--------------------------------${nocol}"
    curl -L $LOGICBLOX_RELEASE > downloads/LogicBlox.tar.gz
    tar xfz downloads/LogicBlox.tar.gz
    if [ -d LogicBlox* ]
    then
        mv LogicBlox* logicblox
    else
        mv logicblox* logicblox
    fi
    popd 1>/dev/null
}


### Main

if [ "$#" = "0" ]
then
    clean
    create_deps
    logicblox
else
    create_deps
    for f in $* ; do
        $f
    done
fi
cleanup
