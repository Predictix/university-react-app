# env.sh (use "source" to run)

# Logicblox
export LB_COMPONENTS=$PWD/deps

source $LB_COMPONENTS/logicblox/etc/profile.d/logicblox.sh
source $LB_COMPONENTS/logicblox/etc/bash_completion.d/logicblox.sh
