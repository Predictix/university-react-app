#! /usr/bin/env python

from lbconfig.api import *

#######################################################################
# 1- Configuration
#

app_name='searchStudent'
core_library='schema'

lbconfig_package(core_library, version='0.1',default_targets=['lb-libraries'])

depends_on(
  logicblox_dep,
  lb_web_dep
)

#######################################################################
# 2- LogiQl Libraries/Projects
#

lb_library(name=core_library,
    srcdir='src',
    deps = ['lb_web']
  )

#######################################################################
# 3- Workspaces
#

run_ws='students'

rule(output = 'all',
  input =['load-sample-data','check-ws-students'],
  phony = True)


#######################################################################
#### 4- Deploy
####

def load_sample_data(ws):
  return [
  'echo "Adding sample students data"',
  'lb web-client import -f -b -i data/students.dlm http://localhost:8080/dw/delim-students',
   'echo "Adding courses data"',
  'lb web-client import -f -b -i data/courses.dlm http://localhost:8080/dw/delim-courses'
  ]

def load_services(ws):
  return['lb web-server load-services -w ' + ws]

rule('load-sample-data', ['check-ws-students','lb-libraries','load-services'],
  load_sample_data(run_ws),
  phony=True)

rule('load-services', 'check-ws-students', load_services(run_ws))

check_lb_workspace(name='students', libraries=[core_library])

rule(output = 'start-nginx', 
    input = [],
    commands = [
        'echo "You can access your application via localhost:3000"',
        'sudo nginx -p $(PWD)/ -c conf/nginx.conf'
        ])