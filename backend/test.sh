#!/bin/bash

set -e

lb create --overwrite students
lb addblock students -f students.logic
lb exec students -f data.logic
